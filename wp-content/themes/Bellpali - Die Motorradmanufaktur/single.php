<!-- get the header template < ?php get_header(); ?> -->
<?php get_header(); ?>

<div class="site-container full-height">



    <div class="card">
        <div class="card-body">
            <div class="image-layout">
                <h2><?php the_title(); ?></h2>
                <?php the_content(); ?>

            </div>
        </div>
    </div>
    <?php

    $images = get_field('gallery'); //add your correct field name

    if ($images) : ?>

        <div class="slider-projekte">

            <?php foreach ($images as $image) : ?>

                <div>
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                </div>

            <?php endforeach; ?>

        </div>

    <?php endif;
    ?>
</div>




<!-- get the footer template < ?php get_footer(); ?> -->
<?php get_footer(); ?>