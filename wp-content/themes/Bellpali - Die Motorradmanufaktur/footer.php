
            <section class="flex-center">
                <nav class="nav-footer">
                    <div>
                        <?php get_template_part('inc/nav_footer'); ?>
                    </div>
                </nav>
            </section>

</div> <!-- closing tag <div class="site-container"> // starts in header.php -->
    <script src="https://cdn.jsdelivr.net/npm/lax.js"></script>
    <script>
        window.onload = function() {
            lax.setup() // init

            const updateLax = () => {
                lax.update(window.scrollY)
                window.requestAnimationFrame(updateLax)
            }

            window.requestAnimationFrame(updateLax)
        }
    </script>

<?php wp_footer(); ?>
    <!-- loads the wp_ footer part -->

</body>

</html>