<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php wp_title(''); ?> - <?php bloginfo('name'); ?></title>

    <?php wp_head(); ?>
    <!-- loads the wp_ head part -->

</head>

<body>

    <div class="site-container-navigation">
        <section>
            
            <div class="display-desktop">
                <nav class="nav-main">
                    <?php get_template_part('inc/nav_main'); ?>
                </nav>
            </div>

            <div class="navigation-position">
                <div class="display-phone burger-position">

                    <div class="burger-line"></div>
                    <div class="burger-line"></div>
                    <div class="burger-line"></div>

                    <?php get_template_part('inc/nav_main'); ?>
                </div>
                
            </div>

        </section>
    </div>

    <div class="site-container">
        <section class="section-header lax" data-lax-opacity="0 1, 50 1, 100 0.5, 150 0" data-lax-translate-y="0 0, 50 -50, 100 -100, 200 -200">
            
                <!-- Display custom header / custom header image -->
                <?php if (get_header_image()) : ?>
                    <div class="flex-center">
                        <a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                            <img src="<?php header_image(); ?>" width="<?php echo absint(get_custom_header()->width); ?>" height="<?php echo absint(get_custom_header()->height); ?>" alt="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>">
                        </a>
                    </div>
                <?php endif; ?>
            
        </section>