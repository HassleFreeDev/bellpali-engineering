<!-- get the header template < ?php get_header(); ?> -->
<?php get_header(); ?>

<div class="site-container">
    <h2>ERROR 404:</h2>
    <p>Die von dir aufgerufene Seite existiert nicht.</p>
</div>

<!-- get the footer template < ?php get_footer(); ?> -->
<?php get_footer(); ?>