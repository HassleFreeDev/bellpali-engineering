<!-- get the header template < ?php get_header(); ?> -->
<?php

$open_text_summer   = get_field('open_text');
$open_time_summer   = get_field('open_time');
$open_text_winter   = get_field('open_text_winter');
$open_time_winter   = get_field('open_time_winter');
$adress             = get_field('adress');
$mail               = get_field('mail');
$phone              = get_field('phone'); ?>


<?php get_header(); ?>

<div class="site-container">
    <div class="container-ml">
        <!-- container-ml = container-margin-left -->
        <section class="section-opening-hours">

            <h3>Öffnungszeiten</h3>
            <div class="container section-container">

                <div class="row">

                    <div class="col-6">
                        <?php echo $open_text_summer; ?>
                    </div>

                    <div class="col-6">
                        <?php echo $open_time_summer; ?>
                    </div>

                </div>
            </div>
            <h3>Winteröffnungszeiten</h3>
            <div class="container section-container">


                <div class="row">

                    <div class="col-6">
                        <?php echo $open_text_winter; ?>
                    </div>

                    <div class="col-6">
                        <?php echo $open_time_winter; ?>
                    </div>

                </div>
            </div>
        </section>

        <section class="section-adress">

            <h3 class="mb">Hier kannst du uns finden</h3>
            <div>
                <?php echo $adress; ?>
            </div>
        </section>

        <section class="section-mail">

            <h3 class="mb">Schreib uns eine Mail und stelle deine Fragen</h3>

            <?php echo $mail; ?>

        </section>

        <section class="section-phone">

            <h3 class="mb">Ruf uns an um einen unverbindlichen Termin zu vereinbaren</h3>

            <?php echo $phone; ?>

        </section>
        <!-- get the footer template < ?php get_footer(); ?> -->
        <?php get_footer(); ?>
    </div>
</div>