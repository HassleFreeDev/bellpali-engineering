"use strict"

jQuery('.slider-projekte').slick({
    infinite: true,
    speed: 400,
    slidesToShow: 3,
      responsive: [{
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
          }
        },
            {
              breakpoint: 800,
              settings: {
                slidesToShow: 1,
              }
            },
    ]
  });