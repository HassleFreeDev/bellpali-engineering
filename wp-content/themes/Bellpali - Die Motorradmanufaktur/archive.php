<!-- get the header template < ?php get_header(); ?> -->
<?php get_header(); ?>

<section class="heading-projekte">
    <h1>Projekte</h1>
</section>

<div class="full-height">
    <?php get_template_part('inc/content', 'projekte'); ?>

    <?php
        global $wp_query;

        $big = 999999999; // need an unlikely integer

        echo paginate_links(array(
            'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
            'format' => '?paged=%#%',
            'current' => max(1, get_query_var('paged')),
            'total' => $wp_query->max_num_pages
        ));
    ?>
</div>

<!-- get the footer template < ?php get_footer(); ?> -->
<?php get_footer(); ?>
 

