<!-- get the header template < ?php get_header(); ?> -->
<?php get_header(); ?>



<div class="site-container">
    <div class="line-container lax" data-lax-translate-y="0 0, 80 -100, 130 -200, 230 -300" data-lax-opacity="0 1, 100 0.5, 180 0">

        <div class="line"></div>

    </div>


    <section class="section-first">
        <div>
            <?php

            $image_first    = get_field('image_first');
            $image_phone    = get_field('image_phone');
            $content_first  = get_field('content_first'); ?>

            <div class="flex-center img-desktop">
                <img class="site-image-fp img-desktop" src="<?php echo esc_url($image_first['url']); ?>" alt="">
            </div>
            <div class="flex-center img-phone">
                <img class="site-image-fp img-phone" src="<?php echo esc_url($image_phone['url']); ?>" alt="">
            </div>

            <div class="p-layout-container">
                <?php echo $content_first; ?>
            </div>

        </div>
    </section>

    <section class="section-second">
        <div>

            <?php

            $image_second = get_field('image_second');
            $image_phone_second = get_field('image_phone_second');
            $content_second = get_field('content_second');  ?>

            <div class="flex-center img-desktop">
                <img class="site-image-fp img-desktop" src="<?php echo esc_url($image_second['url']); ?>" alt="">
            </div>
            <div class="flex-center img-phone">
                <img class="site-image-fp img-phone" src="<?php echo esc_url($image_phone_second['url']); ?>" alt="">
            </div>

            <div class="p-layout-container">
                <?php echo $content_second; ?>
            </div>

        </div>
        <!-- get the footer template < ?php get_footer(); ?> -->


    </section>

    <?php get_footer(); ?>

</div>