<?php

    $args = array(
        'theme_location'    => 'nav_footer',
        'depth'             => 0,
        'container'         => '',
        'menu_class'        => 'navigation-footer' //set an class you can work with css
    );


    wp_nav_menu($args);

    ?>

