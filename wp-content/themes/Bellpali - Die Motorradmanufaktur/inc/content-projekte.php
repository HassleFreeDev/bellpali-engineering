<?php
if (have_posts()) :

    while (have_posts()) : the_post(); ?>
        
            <div class="card">
                <div class="card-body">
                    <div class="image-layout">
                        <?php echo get_the_post_thumbnail(); ?>
                    
                    <h4><?php the_title(); ?></h4></div>

                    <?php the_excerpt(); ?>
                    
                    <div class="btn">
                        <a href="<?php the_permalink(); ?>">Erfahre mehr</a>
                    </div>
                </div>
            </div>

    <?php endwhile; ?>

    <?php else: ?>
        
        <p>Derzeit gibt es keine aktuellen Projekte</p>

<?php endif;
?>

