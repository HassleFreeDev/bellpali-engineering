<?php

// Set google fonts / fonts (Monserrat in this case) tutorial at 'https://www.elmastudio.de/google-fonts-in-wordpress-themes/'

    function bellpali_fonts_url() {
        $fonts_url = '';
    
        /* Translators: If there are characters in your language that are not
        * supported by Monserrat, translate this to 'off'. Do not translate
        * into your own language.
        */
        $montserrat = _x( 'on', 'Montserrat font: on or off', 'bellpali' );
    
        if ( 'off' !== $montserrat ) {
            $font_families = array();
    
            if ( 'off' !== $montserrat ) {
                $font_families[] = 'Montserrat:400,700';
            }
    
            $query_args = array(
                'family' => urlencode( implode( '|', $font_families ) ),
                'subset' => urlencode( 'latin,latin-ext' ),
            );
    
            $fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
        }
    
        return esc_url_raw( $fonts_url );
    }

    function bellpali_scripts_styles() {
        wp_enqueue_style( 'bellpali-fonts', bellpali_fonts_url(), array(), null );
    }
    add_action( 'wp_enqueue_scripts', 'bellpali_scripts_styles' );
//=================================================================================================================================================================================================

// Enque stylesheets to wp_
    function load_stylesheets() {

        wp_register_style('normalize', get_template_directory_uri() . '/css/normalize.css', array(), false, 'all' );
        wp_enqueue_style('normalize');

        wp_register_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), false, 'all' );
        wp_enqueue_style('bootstrap');

        wp_register_style('slick_css', get_template_directory_uri() . '/slick/slick.css', '1.8.1', 'all' );
        wp_enqueue_style('slick_css');

        wp_register_style('slick_theme_css', get_template_directory_uri() . '/slick/slick-theme.css', '1.8.1', 'all' );
        wp_enqueue_style('slick_theme_css');

        wp_register_style('stylesheet', get_template_directory_uri() . '/style.css', '', 1, 'all' );
        wp_enqueue_style('stylesheet');

        wp_register_style('custom_stylesheet', get_template_directory_uri() . '/app.css', '', 1, 'all' );
        wp_enqueue_style('custom_stylesheet');

    }
    add_action('wp_enqueue_scripts', 'load_stylesheets');
//=================================================================================================================================================================================================

// Enque scripts to wp_
    function load_scripts() {

        wp_enqueue_script('jquery'); // wp_ already got jQuery stored in it, so it don't need to be imported or downloaded. It only need to be enqueued!

        wp_register_script('bootstrap_js', get_template_directory_uri() . '/js/bootstrap.min.js', 'jquery', false, true /* third parameter (true, in this case) tells wp_ wether the .js should be placed in the header or in the footer (false = header, true = footer */ );
        wp_enqueue_script('bootstrap_js');

        wp_register_script('slick_js', get_template_directory_uri() . '/slick/slick.min.js', array( 'jquery' ), '1.8.1', true );
        wp_enqueue_script('slick_js');

        wp_register_script('slick_init_js', get_template_directory_uri() . '/js/slick-init.js', array( 'slick_js' ), '1.8.1', true );
        wp_enqueue_script('slick_init_js');

        wp_register_script('custom_js', get_template_directory_uri() . '/app.js', 'jquery', 1, true);
        wp_enqueue_script('custom_js');

        

    }
    add_action('wp_enqueue_scripts', 'load_scripts');
//=================================================================================================================================================================================================

//Adding nav menus

    add_action( 'after_setup_theme', 'register_custom_nav_menus' );

    function register_custom_nav_menus() {
        register_nav_menus( array(
            'nav_main' => 'Hauptnavigation, oben am Bildschirm',
            'nav_footer' => 'Navigation im Footer',
        ) );
    }
//=================================================================================================================================================================================================

// Custom header

    function bellpali_header() {
        $args = array(
            'default-image'      => get_template_directory_uri() . 'img/pebbels.png',
            'default-text-color' => '000',
            'width'              => 1000,
            'height'             => 250,
            'flex-width'         => true,
            'flex-height'        => true,
        );
        add_theme_support( 'custom-header');
    }
    add_action( 'after_setup_theme', 'bellpali_header' );
//=================================================================================================================================================================================================

// Add theme support (different types)

    add_theme_support( 'post-thumbnails' );
//=================================================================================================================================================================================================

// Custom Image sizes

    add_image_size('projekte-slider', 800, 500);